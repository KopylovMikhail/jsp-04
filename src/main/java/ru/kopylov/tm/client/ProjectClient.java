package ru.kopylov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.kopylov.tm.entity.Project;

import java.util.List;

@FeignClient("project")
public interface ProjectClient {

    static ProjectClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Project> getProjects();

    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_XML_VALUE)
    List<Project> getProjectsXml();

    @GetMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Project getProject(@PathVariable("id") String id);

    @GetMapping(value = "/project/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    Project getProjectXml(@PathVariable("id") String id);

    @PutMapping(value = "/project", produces = MediaType.APPLICATION_JSON_VALUE)
    Project updateProject(Project project);

    @PutMapping(value = "/project", produces = MediaType.APPLICATION_XML_VALUE)
    Project updateProjectXml(Project project);

    @DeleteMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteProject(@PathVariable("id") String id);

    @DeleteMapping(value = "/project/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    void deleteProjectXml(@PathVariable("id") String id);

    @PostMapping(value = "/project", produces = MediaType.APPLICATION_JSON_VALUE)
    Project addProject(Project project);

    @PostMapping(value = "/project", produces = MediaType.APPLICATION_XML_VALUE)
    Project addProjectXml(Project project);

}
