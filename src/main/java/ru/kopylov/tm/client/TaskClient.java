package ru.kopylov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.kopylov.tm.entity.Task;

import java.util.List;

@FeignClient("task")
public interface TaskClient {

    static TaskClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> getTasks();

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_XML_VALUE)
    List<Task> getTasksXml();

    @GetMapping(value = "/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Task getTask(@PathVariable("id") String id);

    @GetMapping(value = "/task/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    Task getTaskXml(@PathVariable("id") String id);

    @PutMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
    Task updateTask(Task task);

    @PutMapping(value = "/task", produces = MediaType.APPLICATION_XML_VALUE)
    Task updateTaskXml(Task task);

    @DeleteMapping(value = "/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteTask(@PathVariable("id") String id);

    @DeleteMapping(value = "/task/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    void deleteTaskXml(@PathVariable("id") String id);

    @PostMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
    Task addTask(Task task);

    @PostMapping(value = "/task", produces = MediaType.APPLICATION_XML_VALUE)
    Task addTaskXml(Task task);
    
}
