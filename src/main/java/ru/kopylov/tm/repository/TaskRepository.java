package ru.kopylov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String > {

}
