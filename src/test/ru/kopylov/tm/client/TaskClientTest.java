package ru.kopylov.tm.client;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public class TaskClientTest {

    private final String baseUrl = "http://localhost:8080/";

    private final String testId = "test-id8888";

    private final String testName = "TEST TASK";

    @Before
    public void setUp() throws Exception {
        final Task task = new Task();
        task.setId(testId);
        task.setName(testName);
        TaskClient.client(baseUrl).addTask(task);
    }

    @After
    public void tearDown() throws Exception {
        final Task task = TaskClient.client(baseUrl).getTask(testId);
        if (task != null) TaskClient.client(baseUrl).deleteTask(testId);
    }

    @Test
    public void getTasks() {
        final List<Task> tasks = TaskClient.client(baseUrl).getTasks();
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void getTasksXml() {
        final List<Task> tasks = TaskClient.client(baseUrl).getTasksXml();
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void getTask() {
        final Task task = TaskClient.client(baseUrl).getTask(testId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), testName);
    }

    @Test
    public void updateTask() {
        final Task task = TaskClient.client(baseUrl).getTask(testId);
        String name = "testName";
        task.setName(name);
        TaskClient.client(baseUrl).updateTask(task);
        Assert.assertEquals(TaskClient.client(baseUrl).getTask(testId).getName(), name);
    }

    @Test
    public void deleteTask() {
        Assert.assertNotNull(TaskClient.client(baseUrl).getTask(testId));
        TaskClient.client(baseUrl).deleteTask(testId);
        Assert.assertNull(TaskClient.client(baseUrl).getTask(testId));
    }

    @Test
    public void addTask() {
        final String id = "test-id4444";
        final String name = "nameZ";
        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        Assert.assertEquals(TaskClient.client(baseUrl).addTask(task).getName(), name);
        Assert.assertEquals(TaskClient.client(baseUrl).getTask(id).getId(), id);
        TaskClient.client(baseUrl).deleteTask(id);
    }

}