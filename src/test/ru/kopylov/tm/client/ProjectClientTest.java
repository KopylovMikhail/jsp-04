package ru.kopylov.tm.client;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kopylov.tm.entity.Project;

import java.util.List;

public class ProjectClientTest {

    private final String baseUrl = "http://localhost:8080/";

    private final String testId = "test-id9999";

    private final String testName = "TEST NAME";

    @Before
    public void setUp() throws Exception {
        final Project project = new Project();
        project.setId(testId);
        project.setName(testName);
        ProjectClient.client(baseUrl).addProject(project);
    }

    @After
    public void tearDown() throws Exception {
        final Project project = ProjectClient.client(baseUrl).getProject(testId);
        if (project != null) ProjectClient.client(baseUrl).deleteProject(testId);
    }

    @Test
    public void getProjects() {
        final List<Project> projects = ProjectClient.client(baseUrl).getProjects();
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void getProjectsXml() {
        final List<Project> projects = ProjectClient.client(baseUrl).getProjectsXml();
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void getProject() {
        final Project project = ProjectClient.client(baseUrl).getProject(testId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), testName);
    }

    @Test
    public void updateProject() {
        final Project project = ProjectClient.client(baseUrl).getProject(testId);
        String name = "testName";
        project.setName(name);
        ProjectClient.client(baseUrl).updateProject(project);
        Assert.assertEquals(ProjectClient.client(baseUrl).getProject(testId).getName(), name);
    }

    @Test
    public void deleteProject() {
        Assert.assertNotNull(ProjectClient.client(baseUrl).getProject(testId));
        ProjectClient.client(baseUrl).deleteProject(testId);
        Assert.assertNull(ProjectClient.client(baseUrl).getProject(testId));
    }

    @Test
    public void addProject() {
        final String id = "test-id5555";
        final String name = "nameX";
        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        Assert.assertEquals(ProjectClient.client(baseUrl).addProject(project).getName(), name);
        Assert.assertEquals(ProjectClient.client(baseUrl).getProject(id).getId(), id);
        ProjectClient.client(baseUrl).deleteProject(id);
    }

}